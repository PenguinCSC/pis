#! /bin/bash

# Install FlatHub
apt -y install flatpak gnome-software-plugin-flatpak

# Enable FlatHub Repo
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
