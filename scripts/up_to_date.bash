#! /bin/bash

# Bring System up-to-date

apt update
apt -y upgrade
apt -y dist-upgrade
apt -y autoremove
apt autoclean
