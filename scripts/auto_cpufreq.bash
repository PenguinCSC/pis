#! /bin/bash

## Install Auto CPUFreq Utility

# Clone repository
git clone https://github.com/AdnanHodzic/auto-cpufreq.git

# Install Auto CPUFreq
cd ./auto-cpufreq
./auto-cpufreq-installer --install

# Enable CPUFreq Daemon
systemctl enable auto-cpufreq
systemctl start auto-cpufreq