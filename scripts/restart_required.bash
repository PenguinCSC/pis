#! /bin/bash

## check if restart is needed

if [ -e /var/run/reboot-required ]
then
    echo -e "\e[96;5m!! RESTART REQUIRED !!\e[0m"
else
    echo -e "\e[1mNo Need to Restart :-) \e[0m"
