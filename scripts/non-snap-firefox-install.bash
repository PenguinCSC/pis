#! /bin/bash

## Install the Non-SNAP Firefox on Ubbuntu

# Add PPA Repository
add-apt-repository ppa:mozillateam/ppa

# Install Firefox from the PPA
apt install -t 'o=LP-PPA-mozillateam' firefox

# Enable Automatic Upgrades
mv ./configs/51unattended-upgrades-firefox /etc/apt/apt.conf.d/
mv ./configs/mozillateamppa /etc/apt/preferences.d/