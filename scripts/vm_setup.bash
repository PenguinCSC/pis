#! /bin/bash

## Install KVM and QEMU
apt -y install qemu-kvm libvirt-daemon bridge-utils virtinst libvirt-daemon-system virt-top libguestfs-tools libosinfo-bin  qemu-system virt-manager

# Ask user to Reboot
read -p "Press any key to reboot." -n1 -s
reboot